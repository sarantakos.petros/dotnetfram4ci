﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dotNetFram4CI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Random random = new Random();
            int a = random.Next(0, 1000);
            int b = random.Next(0, 1000);

            Console.WriteLine("Hello world!");
            Console.WriteLine(a + " + " + b + " = " + Sum(a, b));

            Console.Read();
        }

        public static int Sum(int a, int b)
        {
            return a + b;
        }
    }
}
