﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace dotNetFram4CI.Test
{
    [TestClass]
    public class dotNetFram4CITest
    {
        [TestMethod]
        public void MainTest()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void SumTest()
        {
            int result = Program.Sum(4, 5);

            Assert.AreEqual(9, result);
        }
    }
}
